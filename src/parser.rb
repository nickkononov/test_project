class Parser
  require 'nokogiri'
  require 'csv'
  require_relative 'request.rb'

  def initialize(input_url, file_name)
    @csv_file = CSV.open("#{file_name}.csv", "wb")
    @input_url = input_url
  end

  def print_item_info contents
    # parsing item page & add info to csv
    p 'writing to csv...'
    contents.each do |content|
      parsed_content = Nokogiri::HTML(content)
      title = parsed_content.at_xpath('//h1[@class="nombre_producto"]').text.strip
      pic_src_arr = parsed_content.xpath('//ul[@id="thumbs_list_frame"]/li/a/img')
        .map { |link| link.attr('src').gsub!('cart','large') }
      unless (parsed_content.xpath('//ul[@class="attribute_labels_lists"]').empty?) then
        parsed_content
          .xpath('//ul[@class="attribute_labels_lists"]')
          .each_with_index do |_, index|
            weight = parsed_content.xpath('//span[@class="attribute_name"]')[index].text.strip
            price = parsed_content.xpath('//span[@class="attribute_price"]')[index].text.strip
            @csv_file << ["#{title} #{weight}", price, (pic_src_arr[index] || pic_src_arr.first)]
          end
      else
        price = parsed_content.at_xpath('//span[@id="our_price_display"]').text.strip
        @csv_file << [title, price, pic_src_arr]
      end
    end
  end

  def parse
    # getting url after redirect
    site_url = Request.get_single_page(@input_url)
      .head
      .scan(/https?:\S+\//)
      .first || @input_url

    # counting items
    p 'getting number of items ...'
    items_amount = Nokogiri::HTML(Request.get_single_page(site_url).body_str)
      .xpath('//small[@class="heading-counter"]')
      .text
      .gsub(/\D/,'')
      .to_i

    # item list load
    p 'getting items urls...'
    document = Request.get_single_page("#{site_url}?n=#{items_amount}").body_str
    urls = []
    Nokogiri::HTML(document)
      .xpath('//a[@class="lnk_view"]')
      .map { |link| link['href'] }
      .each do |item|
        urls << item
      end
    p 'getting items page bodies...'
    print_item_info(Request.get_multi_pages(urls))
  end

  private :print_item_info

end
