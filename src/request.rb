class Request
  require 'Curb'

  USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'

  def self.get_single_page url
    Curl.get(url) { |c|
     c.ssl_verify_peer = false
     c.headers["User-Agent"] = USER_AGENT}
  end

  def self.get_multi_pages urls
    response_bodies = []

    threads = urls.map do |url|
      Thread.new { response_bodies << Curl.get(url){|c|
        c.ssl_verify_peer = false
        c.headers["User-Agent"] = USER_AGENT}
        .body_str }
    end
    threads.each(&:join)

    response_bodies
  end

end
